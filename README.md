# Elastic Certified Engineer practice environment

## Requirements

- Contents of this repository
- Install `docker` and `docker-compose`

## Installation and usage

- Change `.env` according to your needs
- `cd` into the `./docker-compose` directory
- Run `docker-compose up -d`
- Services available from your local machine:
  - Kibana: http://localhost:5601
  - Elasticsearch: http://localhost:9200
